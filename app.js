/**
 * Module dependencies.
 */

var express = require('express')
 // , routes  = require('./routes')
  , question    = require('./routes/question')
  , flow    = require('./routes/flow')
  , http    = require('http')
  , path    = require('path')
  , expressValidator = require('express-validator') //Declare Express-Validator
  , QuestionProvider = require('./questioncreator').QuestionProvider
  , config = require('./cfg/config');


var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.set('view options', {layout: false});
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(expressValidator);  //required for Express-Validator
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){app.use(express.errorHandler());});

var questionProvider= new QuestionProvider('localhost', 27017);

//Routes

//app.get('/',             function(req,res){questionProvider.findAll(function(error,emps){res.render('index', {title: 'Questions',questions:emps});});});
app.get('/',function(req,res){res.render('home',{title: 'Coach Authoring Tool'});});

app.get('/question/home',function(req,res){res.render('question_home',{title: 'Coach Authoring Tool'});});

app.get('/question/all',function(req,res){questionProvider.findAll(function(error,ques){res.render('question_all', {title: 'Questions',questions:ques});});});

app.get('/question/new',function(req,res) {res.render('question_new',{title: 'New Question',config:config,errors: {}});});

//save new question
app.post('/question/new',function(req,res){
	                     
						  req.assert('questionName','Question Name is required').notEmpty();					  					
                          var errors = req.validationErrors(); 
						  if(!errors)
							{   //No errors were found.  Passed Validation!                                
						      questionProvider.save({questionName: req.param('questionName'),questionType: req.param('questionType'),
								  questionDescription: req.param('questionDescription'),
								  questionCategory: req.param('questionCategory'),
								  questionText0: req.param('questionText0'),
								  questionText1: req.param('questionText1'),
								  questionText2: req.param('questionText2'),
								  questionText3: req.param('questionText3'),
								  icon: req.param('icon'),
								  isQuestionEnabled: req.param('isQuestionEnabled'),
								  id0: req.param('id0'),
								  name0: req.param('name0'),
								  value0: req.param('value0'),
								  state0: req.param('state0'),
								  type0: req.param('type0'),
								  width0: req.param('width0'),
								  id1: req.param('id1'),
								  name1: req.param('name1'),
								  value1: req.param('value1'),
								  state1: req.param('state1'),
								  type1: req.param('type1'),
								  width1: req.param('width1'),
								  id2: req.param('id2'),
								  name2: req.param('name2'),
								  value2: req.param('value2'),
								  state2: req.param('state2'),
								  type2: req.param('type2'),
								  width2: req.param('width2'),
						          id3: req.param('id3'),
								  name3: req.param('name3'),
								  value3: req.param('value3'),
								  state3: req.param('state3'),
								  type3: req.param('type3'),
								  width3: req.param('width3'),
								  id4: req.param('id4'),
								  name4: req.param('name4'),
								  value4: req.param('value4'),
								  state4: req.param('state4'),
								  type4: req.param('type4'),
								  width4: req.param('width4'),
								  id5: req.param('id5'),
								  name5: req.param('name5'),
								  value5: req.param('value5'),
								  state5: req.param('state5'),
								  type5: req.param('type5'),
								  width5: req.param('width5'),
								  id6: req.param('id6'),
								  name6: req.param('name6'),
								  value6: req.param('value6'),
								  state6: req.param('state6'),
								  type6: req.param('type6'),
								  width6: req.param('width6'),
								  id7: req.param('id7'),
								  name7: req.param('name7'),
								  value7: req.param('value7'),
								  state7: req.param('state7'),
								  type7: req.param('type7'),
								  width7: req.param('width7'),
								  id8: req.param('id8'),
								  name8: req.param('name8'),
								  value8: req.param('value8'),
								  state8: req.param('state8'),
								  type8: req.param('type8'),
								  width8: req.param('width8'),
								  id9: req.param('id9'),
								  name9: req.param('name9'),
								  value9: req.param('value9'),
								  state9: req.param('state9'),
								  type9: req.param('type9'),
								  width9: req.param('width9'),
								  id10: req.param('id10'),
								  name10: req.param('name10'),
								  value10: req.param('value10'),
								  state10: req.param('state10'),
								  type10: req.param('type10'),
								  width10: req.param('width10'),
								  questionSpecific: "{"+req.param('questionSpecific')+"}"}, function( err,docs) {															
							if (err)
								  { 
							 res.render('question_new', { title: 'New Question',config:config,errors: err, questionName: req.param('questionName'),
								  questionType: req.param('questionType'),
								  questionDescription: req.param('questionDescription'),
								  questionCategory: req.param('questionCategory'),
								  questionText0: req.param('questionText0'),
								  questionText1: req.param('questionText1'),
								  questionText2: req.param('questionText2'),
								  questionText3: req.param('questionText3'),
								  icon: req.param('icon'),
								  isQuestionEnabled: req.param('isQuestionEnabled'),
								  id0: req.param('id0'),
								  name0: req.param('name0'),
								  value0: req.param('value0'),
								  state0: req.param('state0'),
								  type0: req.param('type0'),
								  width0: req.param('width0'),
								  id1: req.param('id1'),
								  name1: req.param('name1'),
								  value1: req.param('value1'),
								  state1: req.param('state1'),
								  type1: req.param('type1'),
								  width1: req.param('width1'),
								  id2: req.param('id2'),
								  name2: req.param('name2'),
								  value2: req.param('value2'),
								  state2: req.param('state2'),
								  type2: req.param('type2'),
								  width2: req.param('width2'),
						          id3: req.param('id3'),
								  name3: req.param('name3'),
								  value3: req.param('value3'),
								  state3: req.param('state3'),
								  type3: req.param('type3'),
								  width3: req.param('width3'),
								  id4: req.param('id4'),
								  name4: req.param('name4'),
								  value4: req.param('value4'),
								  state4: req.param('state4'),
								  type4: req.param('type4'),
								  width4: req.param('width4'),
								  id5: req.param('id5'),
								  name5: req.param('name5'),
								  value5: req.param('value5'),
								  state5: req.param('state5'),
								  type5: req.param('type5'),
								  width5: req.param('width5'),
								  id6: req.param('id6'),
								  name6: req.param('name6'),
								  value6: req.param('value6'),
								  state6: req.param('state6'),
								  type6: req.param('type6'),
								  width6: req.param('width6'),
								  id7: req.param('id7'),
								  name7: req.param('name7'),
								  value7: req.param('value7'),
								  state7: req.param('state7'),
								  type7: req.param('type7'),
								  width7: req.param('width7'),
								  id8: req.param('id8'),
								  name8: req.param('name8'),
								  value8: req.param('value8'),
								  state8: req.param('state8'),
								  type8: req.param('type8'),
								  width8: req.param('width8'),
								  id9: req.param('id9'),
								  name9: req.param('name9'),
								  value9: req.param('value9'),
								  state9: req.param('state9'),
								  type9: req.param('type9'),
								  width9: req.param('width9'),
								  id10: req.param('id10'),
								  name10: req.param('name10'),
								  value10: req.param('value10'),
								  state10: req.param('state10'),
								  type10: req.param('type10'),
								  width10: req.param('width10'),
								  questionSpecific: req.param('questionSpecific')});}else res.redirect('/question/all')						  
								  });
                            }
						else {   //Display errors to User
                              res.render('question_new', { title: 'New Question',config:config,errors: errors,
								  questionName: req.param('questionName'),
								  questionType: req.param('questionType'),
								  questionDescription: req.param('questionDescription'),
								  questionCategory: req.param('questionCategory'),
								  questionText0: req.param('questionText0'),
								  questionText1: req.param('questionText1'),
								  questionText2: req.param('questionText2'),
								  questionText3: req.param('questionText3'),
								  icon: req.param('icon'),
								  isQuestionEnabled: req.param('isQuestionEnabled'),
								  id0: req.param('id0'),
								  name0: req.param('name0'),
								  value0: req.param('value0'),
								  state0: req.param('state0'),
								  type0: req.param('type0'),
								  width0: req.param('width0'),
								  id1: req.param('id1'),
								  name1: req.param('name1'),
								  value1: req.param('value1'),
								  state1: req.param('state1'),
								  type1: req.param('type1'),
								  width1: req.param('width1'),
								  id2: req.param('id2'),
								  name2: req.param('name2'),
								  value2: req.param('value2'),
								  state2: req.param('state2'),
								  type2: req.param('type2'),
								  width2: req.param('width2'),
						          id3: req.param('id3'),
								  name3: req.param('name3'),
								  value3: req.param('value3'),
								  state3: req.param('state3'),
								  type3: req.param('type3'),
								  width3: req.param('width3'),
								  id4: req.param('id4'),
								  name4: req.param('name4'),
								  value4: req.param('value4'),
								  state4: req.param('state4'),
								  type4: req.param('type4'),
								  width4: req.param('width4'),
								  id5: req.param('id5'),
								  name5: req.param('name5'),
								  value5: req.param('value5'),
								  state5: req.param('state5'),
								  type5: req.param('type5'),
								  width5: req.param('width5'),
								  id6: req.param('id6'),
								  name6: req.param('name6'),
								  value6: req.param('value6'),
								  state6: req.param('state6'),
								  type6: req.param('type6'),
								  width6: req.param('width6'),
								  id7: req.param('id7'),
								  name7: req.param('name7'),
								  value7: req.param('value7'),
								  state7: req.param('state7'),
								  type7: req.param('type7'),
								  width7: req.param('width7'),
								  id8: req.param('id8'),
								  name8: req.param('name8'),
								  value8: req.param('value8'),
								  state8: req.param('state8'),
								  type8: req.param('type8'),
								  width8: req.param('width8'),
								  id9: req.param('id9'),
								  name9: req.param('name9'),
								  value9: req.param('value9'),
								  state9: req.param('state9'),
								  type9: req.param('type9'),
								  width9: req.param('width9'),
								  id10: req.param('id10'),
								  name10: req.param('name10'),
								  value10: req.param('value10'),
								  state10: req.param('state10'),
								  type10: req.param('type10'),
								  width10: req.param('width10'),
								  questionSpecific: req.param('questionSpecific')});
                             }						
						              }
	     );


//update a question
app.get('/question/:id/edit',function(req,res){questionProvider.findById(req.param('_id'), function(errors,question){res.render('question_edit',{question: question,config:config,errors: {}});});});

//save updated question
app.post('/question/:id/edit',function(req,res){
	                      req.assert('questionName'       ,'Question Name is required'       ).notEmpty();						  
                          var errors = req.validationErrors(); 
	                      if(!errors)
							{
	                          questionProvider.update(req.param('_id'),{
								  questionName: req.param('questionName'),
								  questionType: req.param('questionType'),
								  questionDescription: req.param('questionDescription'),
								  questionCategory: req.param('questionCategory'),
								  questionText0: req.param('questionText0'),
								  questionText1: req.param('questionText1'),
								  questionText2: req.param('questionText2'),
								  questionText3: req.param('questionText3'),
								  icon: req.param('icon'),
								  isQuestionEnabled: req.param('isQuestionEnabled'),
								  id0: req.param('id0'),
								  name0: req.param('name0'),
								  value0: req.param('value0'),
								  state0: req.param('state0'),
								  type0: req.param('type0'),
								  width0: req.param('width0'),
								  id1: req.param('id1'),
								  name1: req.param('name1'),
								  value1: req.param('value1'),
								  state1: req.param('state1'),
								  type1: req.param('type1'),
								  width1: req.param('width1'),
								  id2: req.param('id2'),
								  name2: req.param('name2'),
								  value2: req.param('value2'),
								  state2: req.param('state2'),
								  type2: req.param('type2'),
								  width2: req.param('width2'),
						          id3: req.param('id3'),
								  name3: req.param('name3'),
								  value3: req.param('value3'),
								  state3: req.param('state3'),
								  type3: req.param('type3'),
								  width3: req.param('width3'),
								  id4: req.param('id4'),
								  name4: req.param('name4'),
								  value4: req.param('value4'),
								  state4: req.param('state4'),
								  type4: req.param('type4'),
								  width4: req.param('width4'),
								  id5: req.param('id5'),
								  name5: req.param('name5'),
								  value5: req.param('value5'),
								  state5: req.param('state5'),
								  type5: req.param('type5'),
								  width5: req.param('width5'),
								  id6: req.param('id6'),
								  name6: req.param('name6'),
								  value6: req.param('value6'),
								  state6: req.param('state6'),
								  type6: req.param('type6'),
								  width6: req.param('width6'),
								  id7: req.param('id7'),
								  name7: req.param('name7'),
								  value7: req.param('value7'),
								  state7: req.param('state7'),
								  type7: req.param('type7'),
								  width7: req.param('width7'),
								  id8: req.param('id8'),
								  name8: req.param('name8'),
								  value8: req.param('value8'),
								  state8: req.param('state8'),
								  type8: req.param('type8'),
								  width8: req.param('width8'),
								  id9: req.param('id9'),
								  name9: req.param('name9'),
								  value9: req.param('value9'),
								  state9: req.param('state9'),
								  type9: req.param('type9'),
								  width9: req.param('width9'),
								  id10: req.param('id10'),
								  name10: req.param('name10'),
								  value10: req.param('value10'),
								  state10: req.param('state10'),
								  type10: req.param('type10'),
								  width10: req.param('width10'),
								  questionSpecific: req.param('questionSpecific')}
							                      ,function(error,docs){  if (error)
								                                                  { 
													                                  questionProvider.findById(req.param('_id'), function(err,question){res.render('question_edit',{question: question,config:config,errors: error,err: err});});
																				  }
                                                                           else	  res.redirect('/question/all')
																		 });
							}
                          else {   //Display errors to User                                
							    	questionProvider.findById(req.param('_id'), function(err,question){res.render('question_edit',{question: question,config:config,errors: errors,err: err});});
								  
                               }	

											}
		 );

//delete an question
app.post('/question/:id/delete',function(req, res){questionProvider.delete(req.param('_id'),function(error,docs){res.redirect('/question/all')});});

//Export Questions
app.get('/question/export',function(req,res) {questionProvider.export(function(error,questions){res.render('question_export',{title: 'Question Export'})});});


//Import Questions
app.get('/question/import',function(req,res) {res.render('question_import',{title: 'Question Import'});});
//Import Questions
//app.post('/question/import',function(req,res) {questionProvider.import(req,function(err){res.redirect('/')});});
app.post('/question/import',function(req,res) {
	                                              questionProvider.import(req,questionProvider,function(error)
													                            { 
													                                if(error)res.render('question_import_failure',{title: 'Question Import'});
																					else
																					questionProvider.populateQuestions(questionProvider,function(){res.render('question_import_success',{title: 'Question Import'})});
																			    }												 
																		   );
											   }
		  );


//Links for Flows
//Flow Home
app.get('/flow/home',function(req,res){res.render('flow_home',{title: 'Coach Authoring Tool'});});

//View all flows
app.get('/flow/all',function(req,res){questionProvider.findAll_flows(function(error,flw){res.render('flow_all', {title: 'Flows',flows:flw});});});

app.get('/flow/new',function(req,res) {res.render('flow_new',{title: 'New Flow',config:config,errors: {}});});

//save new question
app.post('/flow/new',function(req,res){
	                     
						  req.assert('flowName'            ,'Flow Name is required').notEmpty();

						  req.assert('ruleName_todaysData' ,'Today\'s Data Rule Name is required'            ).notEmpty();
						  req.assert('reward_todaysData'   ,'Today\'s Data Reward is required'               ).notEmpty();
						   req.assert('reward_todaysData'  ,'Today\'s Data Reward has to be a valid integer' ).isInt();
						  req.assert('emitEvent_todaysData','Today\'s Data Emit Event is required'           ).notEmpty();
						 // req.assert('todaysData'          ,'Today\'s Data Value has to be a valid integer'  ).isInt();
                        //  req.assert('todaysDataLength'    ,'Today\'s Data Lenght has to be a valid integer' ).isInt();

						  req.assert('ruleName_weeksData' ,'Week Data Rule Name is required'                      ).notEmpty();
						  req.assert('reward_weeksData'   ,'Week Data Reward is required'                         ).notEmpty();
						  req.assert('reward_weeksData'   ,'Week Data Reward has to be a valid integer'           ).isInt();
						  req.assert('emitEvent_weeksData','Week Data Emit Event is required'                     ).notEmpty();
						 // req.assert('dayInWeek'          ,'Week Data Rule Days in Week has to be a valid integer').isInt();
						//  req.assert('weekLength'         ,'Week Data Length has to be a valid integer'           ).isInt();
                       //   req.assert('dataResult'         ,'Week Data Result has to be a valid integer'           ).isInt();
						  

						  req.assert('ruleName_competitionData' ,'Competition Data Rule Name is required'           ).notEmpty();
						  req.assert('reward_competitionData'   ,'Competition Data Reward is required'              ).notEmpty();
						  req.assert('reward_competitionData'   ,'Competition Data Reward has to be a valid integer').isInt();
						  req.assert('emitEvent_competitionData','Competition Data Emit Event is required'          ).notEmpty();
						  req.assert('usersCompetitionData'     ,'Competition Data UsersCompetitionData is required').notEmpty();
						  req.assert('usersCompetitionData'     ,'Competition Data UsersCompetitionData has to be a valid integer').isInt();
						  
                          var errors = req.validationErrors(); 
						  if(!errors)
							{   //No errors were found.  Passed Validation!                                
						      questionProvider.save_flows({
								  flowName: req.param('flowName'),
								  ruleName_todaysData: req.param('ruleName_todaysData'),reward_todaysData: req.param('reward_todaysData'),emitEvent_todaysData: req.param('emitEvent_todaysData'),todaysData: req.param('todaysData'),todaysData_Condition: req.param('todaysData_Condition'),todaysDataLength: req.param('todaysDataLength'),todaysDataLength_Condition: req.param('todaysDataLength_Condition'),
								  ruleName_weeksData: req.param('ruleName_weeksData'),reward_weeksData: req.param('reward_weeksData'), emitEvent_weeksData: req.param('emitEvent_weeksData'),dayInWeek: req.param('dayInWeek'),weekLength: req.param('weekLength'),dayInWeek_Condition: req.param('dayInWeek_Condition'),lenght_Condition: req.param('lenght_Condition'),dataResult: req.param('dataResult'),dataResult_Condition: req.param('dataResult_Condition'),
								  ruleName_competitionData: req.param('ruleName_competitionData'),reward_competitionData: req.param('reward_competitionData'),emitEvent_competitionData: req.param('emitEvent_competitionData'),usersCompetitionData: req.param('usersCompetitionData'),usersCompetitionData_Condition: req.param('usersCompetitionData_Condition')
								 ,todaysData_Enabled: req.param('todaysData_Enabled'),weeksData_Enabled: req.param('weeksData_Enabled'),competitionData_Enabled: req.param('competitionData_Enabled')
								  
								  }, function( err,docs) {															
							     if (err)
								  {res.render('flow_new', { title: 'New Flow',config:config,errors: err, 
									                        flowName: req.param('flowName'),
								                            ruleName_todaysData: req.param('ruleName_todaysData'),reward_todaysData: req.param('reward_todaysData'),emitEvent_todaysData: req.param('emitEvent_todaysData'),todaysData: req.param('todaysData'),todaysData_Condition: req.param('todaysData_Condition'),todaysDataLength: req.param('todaysDataLength'),todaysDataLength_Condition: req.param('todaysDataLength_Condition'),
								                            ruleName_weeksData: req.param('ruleName_weeksData'),reward_weeksData: req.param('reward_weeksData'), emitEvent_weeksData: req.param('emitEvent_weeksData'),dayInWeek: req.param('dayInWeek'),weekLength: req.param('weekLength'),dayInWeek_Condition: req.param('dayInWeek_Condition'),lenght_Condition: req.param('lenght_Condition'),dataResult: req.param('dataResult'),dataResult_Condition: req.param('dataResult_Condition'),
								                            ruleName_competitionData: req.param('ruleName_competitionData'),reward_competitionData: req.param('reward_competitionData'),emitEvent_competitionData: req.param('emitEvent_competitionData'),usersCompetitionData: req.param('usersCompetitionData'),usersCompetitionData_Condition: req.param('usersCompetitionData_Condition')
								                           ,todaysData_Enabled: req.param('todaysData_Enabled'),weeksData_Enabled: req.param('weeksData_Enabled'),competitionData_Enabled: req.param('competitionData_Enabled')                          
								                           }        
											  );
								   }
							     else res.redirect('/flow/all')						  
								  });
                            }
						else {   //Display errors to User
                              res.render('flow_new', { title: 'New Flow',config:config,errors: errors,
								  flowName: req.param('flowName'),
								  ruleName_todaysData: req.param('ruleName_todaysData'),reward_todaysData: req.param('reward_todaysData'),emitEvent_todaysData: req.param('emitEvent_todaysData'),todaysData: req.param('todaysData'),todaysData_Condition: req.param('todaysData_Condition'),todaysDataLength: req.param('todaysDataLength'),todaysDataLength_Condition: req.param('todaysDataLength_Condition'),
								  ruleName_weeksData: req.param('ruleName_weeksData'),reward_weeksData: req.param('reward_weeksData'), emitEvent_weeksData: req.param('emitEvent_weeksData'),dayInWeek: req.param('dayInWeek'),weekLength: req.param('weekLength'),dayInWeek_Condition: req.param('dayInWeek_Condition'),lenght_Condition: req.param('lenght_Condition'),dataResult: req.param('dataResult'),dataResult_Condition: req.param('dataResult_Condition'),
								  ruleName_competitionData: req.param('ruleName_competitionData'),reward_competitionData: req.param('reward_competitionData'),emitEvent_competitionData: req.param('emitEvent_competitionData'),usersCompetitionData: req.param('usersCompetitionData'),usersCompetitionData_Condition: req.param('usersCompetitionData_Condition')
						         ,todaysData_Enabled: req.param('todaysData_Enabled'),weeksData_Enabled: req.param('weeksData_Enabled'),competitionData_Enabled: req.param('competitionData_Enabled')
								 });
                             }						
						              }
	     );


 //update a flow
app.get('/flow/:id/edit',function(req,res){questionProvider.findById_flows(req.param('_id'), function(errors,flow){res.render('flow_edit',{flow: flow,config:config,errors: {}});});});

//save updated question
app.post('/flow/:id/edit',function(req,res){
	                      req.assert('flowName'            ,'Flow Name is required').notEmpty();

						  req.assert('ruleName_todaysData' ,'Today\'s Data Rule Name is required'            ).notEmpty();
						  req.assert('reward_todaysData'   ,'Today\'s Data Reward is required'               ).notEmpty();
						   req.assert('reward_todaysData'  ,'Today\'s Data Reward has to be a valid integer' ).isInt();
						  req.assert('emitEvent_todaysData','Today\'s Data Emit Event is required'           ).notEmpty();
						//  req.assert('todaysData'          ,'Today\'s Data Value has to be a valid integer'  ).isInt();
                        // req.assert('todaysDataLength'    ,'Today\'s Data Lenght has to be a valid integer' ).isInt();

						  req.assert('ruleName_weeksData' ,'Week Data Rule Name is required'                      ).notEmpty();
						  req.assert('reward_weeksData'   ,'Week Data Reward is required'                         ).notEmpty();
						  req.assert('reward_weeksData'   ,'Week Data Reward has to be a valid integer'           ).isInt();
						  req.assert('emitEvent_weeksData','Week Data Emit Event is required'                     ).notEmpty();
						//  req.assert('dayInWeek'          ,'Week Data Rule Days in Week has to be a valid integer').isInt();
						//  req.assert('weekLength'         ,'Week Data Length has to be a valid integer'           ).isInt();
                        //  req.assert('dataResult'         ,'Week Data Result has to be a valid integer'           ).isInt();
						  

						  req.assert('ruleName_competitionData' ,'Competition Data Rule Name is required'           ).notEmpty();
						  req.assert('reward_competitionData'   ,'Competition Data Reward is required'              ).notEmpty();
						  req.assert('reward_competitionData'   ,'Competition Data Reward has to be a valid integer').isInt();
						  req.assert('emitEvent_competitionData','Competition Data Emit Event is required'          ).notEmpty();
						  req.assert('usersCompetitionData'     ,'Competition Data UsersCompetitionData is required').notEmpty();
						  req.assert('usersCompetitionData'     ,'Competition Data UsersCompetitionData has to be a valid integer').isInt();						  
                          var errors = req.validationErrors(); 
	                      if(!errors)
							{
	                          questionProvider.update_flows(req.param('_id'),{
								  flowName: req.param('flowName'),
								  ruleName_todaysData: req.param('ruleName_todaysData'),reward_todaysData: req.param('reward_todaysData'),emitEvent_todaysData: req.param('emitEvent_todaysData'),todaysData: req.param('todaysData'),todaysData_Condition: req.param('todaysData_Condition'),todaysDataLength: req.param('todaysDataLength'),todaysDataLength_Condition: req.param('todaysDataLength_Condition'),
								  ruleName_weeksData: req.param('ruleName_weeksData'),reward_weeksData: req.param('reward_weeksData'), emitEvent_weeksData: req.param('emitEvent_weeksData'),dayInWeek: req.param('dayInWeek'),weekLength: req.param('weekLength'),dayInWeek_Condition: req.param('dayInWeek_Condition'),lenght_Condition: req.param('lenght_Condition'),dataResult: req.param('dataResult'),dataResult_Condition: req.param('dataResult_Condition'),
								  ruleName_competitionData: req.param('ruleName_competitionData'),reward_competitionData: req.param('reward_competitionData'),emitEvent_competitionData: req.param('emitEvent_competitionData'),usersCompetitionData: req.param('usersCompetitionData'),usersCompetitionData_Condition: req.param('usersCompetitionData_Condition')
						         ,todaysData_Enabled: req.param('todaysData_Enabled'),weeksData_Enabled: req.param('weeksData_Enabled'),competitionData_Enabled: req.param('competitionData_Enabled')
								 
								  }
							                      ,function(error,docs){  if (error)
								                                                  { 
													                                 
																					  questionProvider.findById_flows(req.param('_id'), function(err,flow){res.render('flow_edit',{flow: flow,config:config,errors: error,err: err});});
																				  }
                                                                           else	  res.redirect('/flow/all')
																		 });
							}
                          else {   //Display errors to User                                
							    	questionProvider.findById_flows(req.param('_id'), function(err,flow){res.render('flow_edit',{flow: flow,config:config,errors: errors,err: err});});
								  
                               }	

											}
		 );



//delete an flow
app.post('/flow/:id/delete',function(req, res){questionProvider.delete_flows(req.param('_id'),function(error,docs){res.redirect('/flow/all')});});

//Export Flows
app.get('/flow/:id/export',function(req,res) {questionProvider.export_flow(req.param('_id'),function(error,flows){res.render('flow_export',{title: 'Flow Export'})});});



//New Dynamic flow

app.get('/flow/libraries',function(req,res){questionProvider.getLibraries(function(error,lbr){res.render('flow_library', {title: 'Flows',libraries:lbr});});});


app.post('/flow/categories',function(req,res){questionProvider.getCategoriesFromLibrary(req.param('libraryName'),function(error,ctgr){res.render('flow_category', {title: 'Flows',categories:ctgr,library:req.param('libraryName')});});});

app.post('/flow/function',function(req,res){questionProvider.getFunctionsFromCategory(req.param('libraryName'),req.param('categoryName'),function(error,fnNames){res.render('flow_function', {title: 'Flows',categories:req.param('categoryName'),library:req.param('libraryName'),functionName:fnNames});});});

app.post('/flow/function/param',function(req,res){questionProvider.getFunctionsParams(req.param('libraryName'),req.param('categoryName'),req.param('functionName'),function(error,param,paramType,defaultValue){res.render('flow_param', {title: 'Flows',categories:req.param('categoryName'),library:req.param('libraryName'),functionName:req.param('functionName'),paramName:param,paramType:paramType,defaultValue:defaultValue,config:config});});});

//app.get('/flow/newdynamic',function(req,res) {res.render('flow_newdynamic',{title: 'New Flow',categories:req.param('categoryName'),library:req.param('libraryName'),functionName:req.param('functionName'),errors: {}});});

app.listen(3000);