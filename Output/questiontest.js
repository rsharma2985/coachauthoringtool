var utils = require('../lib/utils');
var middleware = require('middleware');
var BaseQuestion = middleware.models.BaseQuestion;

var questions = utils.map(BaseQuestion, [
{header : {
            questionId:          0,
            questionName:       'Test',
            questionType:       'demographics',
            questionDescription:'Activity',
            questionCategory:   'ABOUT YOU',
            questionText0:      ' ',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {   test : 'test123'}
}
,
{header : {
            questionId:          1,
            questionName:       'Test2',
            questionType:       'demographics',
            questionDescription:'Activity',
            questionCategory:   'ABOUT YOU',
            questionText0:      '   ',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {   Key:'value'      }
}
,
{header : {
            questionId:          2,
            questionName:       'Greeting',
            questionType:       'greeting',
            questionDescription:'Intro Screen',
            questionCategory:   'ABOUT YOU',
            questionText0:      'Hey there, ',
            questionText1:      'Glad you are here! I am Coach H!',
            questionText2:      'I know you are interested in your health so let\'s get started. The following are a series of questions which will help me compile your personal health profile.',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: { extraText: [ { point: '!' } ] }
}
,
{header : {
            questionId:          3,
            questionName:       'BloodGlucoseStep4',
            questionType:       'singleselectquestion',
            questionDescription:'Blood Glucose',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your diabetes?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-bloodSugar-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          4,
            questionName:       'Hypertension',
            questionType:       'singleselectquestion',
            questionDescription:'Hypertension',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has a doctor asked you to check your blood pressure regularly? How often?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Daily',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Weekly',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Bi-weekly',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '3',
            name: 'Monthly',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '4',
            name: 'Other',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          5,
            questionName:       'DietStep2',
            questionType:       'inspiration',
            questionDescription:'Diet',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Which diet?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'DietStep2',
            value:'',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          6,
            questionName:       'Obesity',
            questionType:       'singleselectquestion',
            questionDescription:'Obesity',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your obesity?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          7,
            questionName:       'DietStep5',
            questionType:       'singleselectquestion',
            questionDescription:'Diet',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Do you limit the amount of salt in your regular diet?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          8,
            questionName:       'Activity',
            questionType:       'singleselectlikert',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'How many days a week do you do moderate-intensity exercise? (Moderate-intensity exercises include anything that increases your breathing or heart rate for 30 minutes or more in one day — like brisk walking, jogging, or using aerobic machines at the gym.)',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-activity-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: '0',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          9,
            questionName:       'Evaluation',
            questionType:       'evaluation',
            questionDescription:'Your Custom Evaluation',
            questionCategory:   'YOUR PLAN',
            questionText0:      'Thanks for all the information, ',
            questionText1:      '! Based on what I heard, here’s a basic profile of your health and habits. ',
            questionText2:      'You\'ll see that I’ve marked the things I think you should focus on for the first 90 days of our work together.  Take a look and see what you think. Remember, you can always change these areas of focus later.',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: { evals: 
   [ { label: 'At Risk',
       icon: 'portal/css/theme/standard/img/h-flag-risk.png',
       entries: 
        [ { icon: 'portal/css/theme/standard/img/h-icon-physicalActivity.png',
            value: 'Physical Activity' },
          { icon: 'portal/css/theme/standard/img/h-icon-nutrition.png',
            value: 'Nutrition' } ] },
     { label: 'Needs Work',
       icon: 'portal/css/theme/standard/img/h-flag-work.png',
       entries: 
        [ { icon: 'portal/css/theme/standard/img/h-icon-medication.png',
            value: 'Medication' },
          { icon: 'portal/css/theme/standard/img/h-icon-sleep.png',
            value: 'Sleep' },
          { icon: 'portal/css/theme/standard/img/h-icon-weight.png',
            value: 'Weight' } ] },
     { label: 'Okay',
       icon: 'portal/css/theme/standard/img/h-flag-okay.png',
       entries: 
        [ { icon: 'portal/css/theme/standard/img/h-icon-stress.png',
            value: 'Stress' },
          { icon: 'portal/css/theme/standard/img/h-icon-hydration.png',
            value: 'Hydration' } ] },
     { label: 'Great',
       icon: 'portal/css/theme/standard/img/h-flag-great.png',
       entries: 
        [ { icon: 'portal/css/theme/standard/img/h-icon-mood.png',
            value: 'Mood' },
          { icon: 'portal/css/theme/standard/img/h-icon-alcohol.png',
            value: 'Alcohol in moderation' },
          { icon: 'portal/css/theme/standard/img/h-icon-smoking.png',
            value: 'NO TOBACCO' } ] } ] }
}
,
{header : {
            questionId:          10,
            questionName:       'NoWay',
            questionType:       'greeting',
            questionDescription:'No Way',
            questionCategory:   'Homepage Rules',
            questionText0:      'No problem.  You achieved your goal, you should feel good about that.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          11,
            questionName:       'ChallengeMetTodayDouble',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work, you surpassed today\'s challenge goal of 5,000 steps by double or more.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          12,
            questionName:       'EvaluateUserDevices',
            questionType:       'greeting',
            questionDescription:'Evaluate User Devices',
            questionCategory:   'Homepage Rules',
            questionText0:      '',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          13,
            questionName:       'ChallengeMetToday3',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Amazing! You\'ve hit three times your goal just in one day! Another kudo for your hard work!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          14,
            questionName:       'ChallengeMetToday8',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Stupendous! You keep surprising us! Eight times your goal today and counting! Another kudo to you!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          15,
            questionName:       'Nutrition1',
            questionType:       'singleselectlikert',
            questionDescription:'Nutrition',
            questionCategory:   'YOUR HABITS',
            questionText0:      'On average, how many days per week do you eat high-fat red meat?',
            questionText1:      'High-fat meats include some hamburger meats, sausages, and some deli and processed meats.',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'None',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'7',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          16,
            questionName:       'TimeFor6WeekAssessment',
            questionType:       'greeting',
            questionDescription:'Six Week Assessment',
            questionCategory:   'Homepage Rules',
            questionText0:      'Now that you\'ve been using Empower for 6 weeks, let\'s take another look at how you\'re doing!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          17,
            questionName:       'AwardHsaSleepWeight',
            questionType:       'greeting',
            questionDescription:'Activity',
            questionCategory:   'Homepage Rules',
            questionText0:      'Congratulations! You are sleeping better and losing weight!',
            questionText1:      'A deposit has been made into your HSA account!',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}                    
}
,
{header : {
            questionId:          18,
            questionName:       'ChronicIllnesses',
            questionType:       'multiselectquestion',
            questionDescription:'Chronic Illnesses',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Have you ever had a diagnosis of any of these conditions?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Type II Diabetes',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '1',
            name: 'Heart Disease',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '2',
            name: 'Hypertension',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '3',
            name: 'High Cholesterol',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '4',
            name: 'Metabolic Syndrome',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '5',
            name: 'Sleep Apnea',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '6',
            name: 'Depression',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '7',
            name: 'None',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          19,
            questionName:       'BloodGlucoseStep5',
            questionType:       'singleselectquestion',
            questionDescription:'Blood Glucose',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'How frequently are you supposed to take your medication?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-bloodSugar-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Weekly',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Daily',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: '2x per day',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ,{
            id:   '3',
            name: '3x per day',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          20,
            questionName:       'Aspirin',
            questionType:       'singleselectquestion',
            questionDescription:'Aspirin',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Do you take a daily aspirin or baby aspirin?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          21,
            questionName:       'HeartDisease',
            questionType:       'singleselectquestion',
            questionDescription:'Heart Disease',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your heart disease?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          22,
            questionName:       'Medication',
            questionType:       'singleselectlikert',
            questionDescription:'Medication',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'How many prescribed medicines are you taking now?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-activity-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: '1',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '2',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '3',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '4',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '5+',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          23,
            questionName:       'OldActivity',
            questionType:       'singleselectquestion',
            questionDescription:'Activity',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'In general, how many days a week do you do some kind of moderate-intensity exercise? (Moderate-intensity activities include anything that increases your breathing or heart rate for 30 minutes or more in one day — like brisk walking, jogging, or using aerobic machines at the gym.)',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-activity-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Not Very Often',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Every Day',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          24,
            questionName:       'ActivityStep2',
            questionType:       'singleselectlikert',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'On those days how many minutes do you exercise?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-activity-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: '< 10',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: '10',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: '20',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '3',
            name: '30',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '4',
            name: '40',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '5',
            name: '50',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '6',
            name: '60',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '7',
            name: '> 60',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          25,
            questionName:       'Plan',
            questionType:       'plan',
            questionDescription:'Your Custom Plan',
            questionCategory:   'YOUR PLAN',
            questionText0:      'Great! Here\'s the plan I’ve set up for you. I hope you’ll take a minute to congratulate yourself — setting up your plan is a big step.',
            questionText1:      'After the first 90 days, we\'ll look at this again and check your progress. Then you can decide if you want to make any changes. ',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          26,
            questionName:       'Cholesterol',
            questionType:       'singleselectquestion',
            questionDescription:'Cholesterol',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'How often would you say you measure your cholesterol?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Not Very Often',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Every Day',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          27,
            questionName:       'SuccessfulOnboarding',
            questionType:       'greeting',
            questionDescription:'Onboarding Completion',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work, you completed Onboarding and have earned your first 20 Kudos!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          28,
            questionName:       'MinutesMetToday',
            questionType:       'greeting',
            questionDescription:'Activity Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work, you attained today\'s challenge goal of 15 minutes.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          29,
            questionName:       'ChallengeMetToday4',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Wow, you\'re really on a roll! You\'ve hit four times your goal today! Yet another kudo for you!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          30,
            questionName:       'ChallengeMetToday9',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Magnificent! You\'ve achieved nine times your goal today. What a champion! Yet another kudo for you!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          31,
            questionName:       'Nutrition2',
            questionType:       'singleselectlikert',
            questionDescription:'Nutrition',
            questionCategory:   'YOUR HABITS',
            questionText0:      'On average, how many days per week do you eat at least 5 servings of fruits and vegetables?',
            questionText1:      'One serving of fruit is the size of a peach or medium apple.  One serving of vegetables is ½ cup.  Please note that potatoes don’t count – they are a starch. Also, if you eat a big salad you may be getting several servings of vegetables in one meal!',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'None',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'7',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          32,
            questionName:       'GenericGreeting',
            questionType:       'greeting',
            questionDescription:'Generic Greeting',
            questionCategory:   'Homepage Rules',
            questionText0:      '',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          33,
            questionName:       'LowActivityStaticWeight2',
            questionType:       'greeting',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'I see you are having trouble with your activity challenge and this may be affecting your ability to lose weight.',
            questionText1:      'Perhaps a sleep challenge would be helpful?',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          34,
            questionName:       'BloodGlucose',
            questionType:       'singleselectquestion',
            questionDescription:'Blood Glucose',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor instructed you to test your blood glucose regularly?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-bloodSugar-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          35,
            questionName:       'BloodGlucoseStep6',
            questionType:       'singleselectquestion',
            questionDescription:'Blood Glucose',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'How frequently do you take actually take your prescribed medicines?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Never',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: '2 Days A Week Compliant',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: '5 Days A Week Compliant',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ,{
            id:   '3',
            name: '7 Days A Week Compliant',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          36,
            questionName:       'BMI',
            questionType:       'singleselectquestion',
            questionDescription:'BMI',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Do you know what your body mass index is?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          37,
            questionName:       'HypertensionStep2',
            questionType:       'singleselectquestion',
            questionDescription:'Hypertension',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your hypertension?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          38,
            questionName:       'Obesity',
            questionType:       'singleselectquestion',
            questionDescription:'Obesity',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your obesity?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          39,
            questionName:       'Aspirations',
            questionType:       'multiselectquestion',
            questionDescription:'Aspirations',
            questionCategory:   'YOUR GOALS',
            questionText0:      'What would you like to accomplish? Do you want to...',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Lose weight',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '1',
            name: 'Have more energy',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '2',
            name: 'Feel better about myself',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '3',
            name: 'Make someone I care about proud',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '4',
            name: 'Improve my health',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '5',
            name: 'Complete a fitness event/challenge',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '6',
            name: 'Be a role model',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ,{
            id:   '7',
            name: 'Reduce my stress',
            value:'',
            state:'',
            type: 'boolean',
            width:'26%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          40,
            questionName:       'ActivityStep3',
            questionType:       'singleselectlikert',
            questionDescription:'ActivityStep3',
            questionCategory:   'YOUR HABITS',
            questionText0:      'About how many times a week do you do that?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-activity-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: '1',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '2',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '3',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '4',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '5',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '6',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '7',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          41,
            questionName:       'ChallengeMet',
            questionType:       'singleselectquestion',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work, you attained today\'s challenge goal of 5,000 steps.',
            questionText1:      'Think you\'ve got another 1,000 steps in you today?',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Easily',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Hopefully',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'NoWay',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          42,
            questionName:       'HeartRate',
            questionType:       'singleselectquestion',
            questionDescription:'Heart Rate',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'How often would you say you measure your resting pulse?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Not Very Often',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Every Day',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          43,
            questionName:       'Fitbit',
            questionType:       'greeting',
            questionDescription:'Fitbit Registration',
            questionCategory:   'Homepage Rules',
            questionText0:      'Congratulations! You have successfully registered your Fitbit Device!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          44,
            questionName:       'MinutesMetTodayDouble',
            questionType:       'greeting',
            questionDescription:'Activity Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work, you surpassed today\'s challenge goal of 15 minutes steps by double or more.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          45,
            questionName:       'ChallengeMetToday5',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Wowzers, you\'ve hit five times your goal today! We\'ll keep the kudos coming!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          46,
            questionName:       'ChallengeMetToday10',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Extraordinary! You\'ve done ten times your goal today, I\'m speechless!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          47,
            questionName:       'Training',
            questionType:       'singleselectlikert',
            questionDescription:'Training',
            questionCategory:   'YOUR HABITS',
            questionText0:      'On average, how many days per week do you participate in resistance training?',
            questionText1:      'Resistance training builds the strength, endurance and size of skeletal muscles through the use of free weights, resistance rubber bands, load carrying exercises and other options.',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'None',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'7',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          48,
            questionName:       'StaticWeight',
            questionType:       'greeting',
            questionDescription:'Generic Greeting',
            questionCategory:   'Homepage Rules',
            questionText0:      'You\'re doing great with your activity goals! Still no weight loss? Try keeping track of the calories you consume daily and strive to take in fewer calories than you burn.',
            questionText1:      'You can use the handy calorie counter found on your Health Profile view to track your daily calorie intake.',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          49,
            questionName:       'LoseWeight',
            questionType:       'greeting',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'Congratulations! Your sleeping has improved, especially during the last week! And you\'ve hit your activity goal on at least 5 days a week for the last three weeks! Keep it up!',
            questionText1:      'You\'re doing great with your weight loss! You\'ve had a net loss of 2% of your starting weight. In recognition of your great effort, you get an extra $100 in your HSA account!',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          50,
            questionName:       'BloodGlucoseStep2',
            questionType:       'singleselectquestion',
            questionDescription:'Blood Glucose',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'How frequently has your doctor recommended that you test your blood glucose?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-bloodSugar-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Daily',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Before and after meals',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Before and after one meal per day',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          51,
            questionName:       'Stress',
            questionType:       'singleselectquestion',
            questionDescription:'Stress',
            questionCategory:   'YOUR HABITS',
            questionText0:      'During the past week, how stressed out were you?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Not at all',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'A little',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Somewhat',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '3',
            name: 'Very!',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          52,
            questionName:       'BMIStep2',
            questionType:       'inspiration',
            questionDescription:'BMI',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Please enter you body mass index.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'BMIStep2',
            value:'',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          53,
            questionName:       'CholesterolStep2',
            questionType:       'singleselectquestion',
            questionDescription:'Cholesterol',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your high cholesterol?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          54,
            questionName:       'DietStep3',
            questionType:       'singleselectquestion',
            questionDescription:'Diet',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Does your regular diet include more than two days of eating high-fat meat?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          55,
            questionName:       'Inspiration',
            questionType:       'inspiration',
            questionDescription:'Inspiration',
            questionCategory:   'YOUR GOALS',
            questionText0:      'Great! Can you tell me a little bit about why you are doing this? What is inspiring you to seek better health? (You will see this sentence on your home page every time you log on.)',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Inspiration',
            value:'',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          56,
            questionName:       'Hydration',
            questionType:       'singleselectlikert',
            questionDescription:'Hydration',
            questionCategory:   'YOUR HABITS',
            questionText0:      'How many glasses of water do you drink on a typical day?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'None',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'7',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '8',
            name: '8',
            value:'8',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '9',
            name: 'More',
            value:'9',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          57,
            questionName:       'Easily',
            questionType:       'greeting',
            questionDescription:'Do More',
            questionCategory:   'Homepage Rules',
            questionText0:      'Keep pushing let\'s see what you can accomplish.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          58,
            questionName:       'HeartDisease',
            questionType:       'singleselectquestion',
            questionDescription:'Heart Disease',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor instructed you to monitor your resting heart rate?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          59,
            questionName:       'WelcomeTour',
            questionType:       'greeting',
            questionDescription:'Coach Welcome Tour',
            questionCategory:   'Homepage Rules',
            questionText0:      'I am your personal coach. When you need me, I will be there to help!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          60,
            questionName:       'YourTeamAhead',
            questionType:       'greeting',
            questionDescription:'Team Competition',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work! You team is ahead in the competition!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          61,
            questionName:       'ChallengeMetToday6',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Six times your goal already? We are more than six times impressed! Another kudo!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          62,
            questionName:       'ChallengeMetToday11',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Truly inspirational! You\'ve gone above and beyond today by hitting eleven times your goal! Another kudo to you!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          63,
            questionName:       'FitbitError',
            questionType:       'greeting',
            questionDescription:'Fitbit Registration',
            questionCategory:   'Homepage Rules',
            questionText0:      'Looks like your device was not mapped. Check your Fitbit credentials and try again.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          64,
            questionName:       'LowActivityStaticWeight',
            questionType:       'singleselectlikert',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'Sorry - you\'ve only met your activity goals once a week, and come close on three days a week.',
            questionText1:      'What\'s the one thing you would do over the next three weeks that would help you get more active and lose weight?',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Draw up a set schedule',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: 'Make a commitment with a friend',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: 'Get to bed on time every night',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          65,
            questionName:       'LoseWeightCalories',
            questionType:       'greeting',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'Fantastic! Since you\'ve been tracking your calories, you\'ve started to lose weight. You\'ve lost 3 pounds in 3 weeks!',
            questionText1:      'Wow! You\'ve entered your calorie count 7 times a week, and you\'re staying within your healthy range! To recognize your accomplishment, I\'m awarding you 50 additional kudos! Way to go!',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          66,
            questionName:       'BloodGlucoseStep3',
            questionType:       'singleselectquestion',
            questionDescription:'Blood Glucose',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Currently, on average how frequently do you test your blood glucose?',
            questionText1:      '',
            questionText2:      '',
            icon:               'h-icon-bloodSugar-ob',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Less than once a week',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Every few days',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'At-least once per day',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ,{
            id:   '3',
            name: 'Before and after one meal each day',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ,{
            id:   '4',
            name: 'Before and after meals',
            value:'0',
            state:'',
            type: 'radio',
            width:'65%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          67,
            questionName:       'HeartDisease',
            questionType:       'singleselectquestion',
            questionDescription:'Heart Disease',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'For which heart disease do you have a diagnosis?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Arrhythmia',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'Cardiomyopathy',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Coronary Artery Disease',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '3',
            name: 'Heart Failure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '4',
            name: 'Other',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          68,
            questionName:       'Diet',
            questionType:       'singleselectquestion',
            questionDescription:'Diet',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has a doctor recommended a particular diet to help you lose weight?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          69,
            questionName:       'MetabolicSyndrome',
            questionType:       'singleselectquestion',
            questionDescription:'Metabolic Syndrome',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Has your doctor prescribed medication related to your metabolic syndrome?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '2',
            name: 'Unsure',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          70,
            questionName:       'DietStep4',
            questionType:       'singleselectquestion',
            questionDescription:'Diet',
            questionCategory:   'YOUR HEALTH',
            questionText0:      'Do you eat at least 5 fruits and/or vegetables a day?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'Yes',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ,{
            id:   '1',
            name: 'No',
            value:'0',
            state:'',
            type: 'radio',
            width:'40%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          71,
            questionName:       'Demographics',
            questionType:       'demographics',
            questionDescription:'User Specific Info',
            questionCategory:   'ABOUT YOU',
            questionText0:      'Let\'s start with the basics! ',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: { areYou: 
   { label: 'Are you...',
     choice1: { id: '0', label: 'female', type: 'radio', value: 'checked' },
     choice2: { id: '1', label: 'male', type: 'radio', value: '' } },
  whenWere: 
   { label: 'When were you born?',
     field: { id: '2', type: 'date', units: 'mm/dd/yy', value: '' } },
  howTall: 
   { label: 'How tall are you?',
     choice1: { id: '3', label: 'ft', type: 'number', value: '' },
     choice2: { id: '4', label: 'in', type: 'number', value: '' } },
  muchWeigh: 
   { label: 'How much do you weigh?',
     field: { id: '5', label: 'lbs', type: 'number', value: '' } } }
}
,
{header : {
            questionId:          72,
            questionName:       'Sleep',
            questionType:       'singleselectlikert',
            questionDescription:'Sleep',
            questionCategory:   'YOUR HABITS',
            questionText0:      'How many nights a week do you sleep for at least 7 hours?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'None',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'7',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          73,
            questionName:       'Hopefully',
            questionType:       'greeting',
            questionDescription:'Hopeful',
            questionCategory:   'Homepage Rules',
            questionText0:      'You\'ve hit your goal for today, anything extra is icing on the cake.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          74,
            questionName:       'ChallengeMetToday',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Nice work, you attained today\'s challenge goal of 5,000 steps.',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          75,
            questionName:       'EvaluateRiskGrid',
            questionType:       'greeting',
            questionDescription:'Evaluate Risk Grid',
            questionCategory:   'Homepage Rules',
            questionText0:      '',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          76,
            questionName:       'YourTeamBehind',
            questionType:       'greeting',
            questionDescription:'Team Competition',
            questionCategory:   'Homepage Rules',
            questionText0:      'Hey! Your team is behind in the competition! Better hustle!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          77,
            questionName:       'ChallengeMetToday7',
            questionType:       'greeting',
            questionDescription:'Steps Challenge Daily Goal Met',
            questionCategory:   'Homepage Rules',
            questionText0:      'Seven\'s a lucky number, and you\'ve hit seven times your goal! You are so healthrageous! How about another kudo?',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          78,
            questionName:       'Tobacco',
            questionType:       'singleselectlikert',
            questionDescription:'Tobacco',
            questionCategory:   'YOUR HABITS',
            questionText0:      'On average, how many days in a typical week do you smoke or use other tobacco products?',
            questionText1:      'When used over a long period, tobacco and related chemicals such as tar and nicotine can increase your risk for many health problems.',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [
        {
            id:   '0',
            name: 'None',
            value:'0',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '1',
            name: '1',
            value:'1',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '2',
            name: '2',
            value:'2',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '3',
            name: '3',
            value:'3',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '4',
            name: '4',
            value:'4',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '5',
            name: '5',
            value:'5',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '6',
            name: '6',
            value:'6',
            state:'',
            type: 'text',
            width:'60%'
        }
       ,{
            id:   '7',
            name: '7',
            value:'7',
            state:'',
            type: 'text',
            width:'60%'
        }
       ],
 questionSpecific: {}
}
,
{header : {
            questionId:          79,
            questionName:       'TimeFor3WeekAssessment',
            questionType:       'greeting',
            questionDescription:'Three Week Assessment',
            questionCategory:   'Homepage Rules',
            questionText0:      'Now that you\'ve been using Empower for 3 weeks, let\'s take a look at how you\'re doing!',
            questionText1:      '',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          80,
            questionName:       'AwardKudosCalories',
            questionType:       'greeting',
            questionDescription:'Award Kudos',
            questionCategory:   'Homepage Rules',
            questionText0:      'Congratulations! Counting calories worked!',
            questionText1:      'Here are 50 Kudos to spend as you wish!',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
,
{header : {
            questionId:          81,
            questionName:       'SuggestSleepChallenge',
            questionType:       'greeting',
            questionDescription:'Activity',
            questionCategory:   'YOUR HABITS',
            questionText0:      'How\'s your sleep lately? Studies show people who slept five or fewer hours a night were 1.5 times more likely to become obese.',
            questionText1:      'Why not take the sleep challenge? Click on Add Challenges, activate the \'Sleep On It\' challenge and start getting the sleep you need!',
            questionText2:      '',
            icon:               '',
            isQuestionEnabled:   true
          },
 data: [],
 questionSpecific: {}
}
]);

module.exports.questions = questions;