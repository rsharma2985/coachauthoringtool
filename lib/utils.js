var _ = require('underscore');

module.exports.map = function(Model, items) {
    var models = _.map(items, function(item) {
        return new Model(item);
    });
    return models;
};