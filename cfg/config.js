


module.exports.DROPDOWN_DATA = 
{ 


QUESTION_DATA : 
	             {   
	                questionTypes        : ['demographics', 'evaluation', 'greeting', 'inspiration', 'multiselectquestion', 'plan',  'singleselectlikert', 'singleselectquestion'], 
	                questiondescriptions : ['Activity','Activity Challenge Daily Goal Met','ActivityStep2','Aspirations','Aspirin','Blood Glucose','BMI','Cholesterol','Chronic Illnesses','Coach Welcome Tour','Diet','Do More','Evaluate Risk Grid','Evaluate User Devices','Fitbit Registration','Heart Disease','Heart Rate','Hopeful','Hydration','Hypertension','Inspiration','Intro Screen','Medication','Metabolic Syndrome','No Way','Obesity','Onboarding Completion','Sleep','Steps Challenge Daily Goal Met','Stress','Team Competition','User Specific Info','Your Custom Evaluation','Your Custom Plan'], 
                    questionCategories   : ['ABOUT YOU', 'Homepage Rules', 'YOUR GOALS', 'YOUR HABITS', 'YOUR HEALTH', 'YOUR PLAN'],
	                types                : ['boolean', 'date', 'number', 'radio','text'] 
	             },
COMMONN_DATA :
	             {
	                 boolean_data        : ['true','false'],
	                 condition           : [ '==','!=','>','<','>=','<='] 
	             },
FLOW_DATA :
	             {
	                 emit_event          : ['challenge.goal.achieved']	                 
	             }
};