var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var baseQuestionsSchema = new Schema({
        header: {
            questionId: Number,
            questionName: String,
            questionType: String,
            questionDescription: String,
            questionCategory: String,
            questionText0: String,
            questionText1: String,
            questionText2: String,
            questionText3: String,
            icon: String,
            isQuestionEnabled: Boolean
        },
        data: [Schema.Types.Mixed],
        questionSpecific: Schema.Types.Mixed
    });

module.exports = mongoose.model('BaseQuestion', baseQuestionsSchema);
