var Db         = require('mongodb').Db;
var Connection = require('mongodb').Connection;
var Server     = require('mongodb').Server;
var BSON       = require('mongodb').BSON;
var ObjectID   = require('mongodb').ObjectID;
var fileStream = require('fs');
var question   = require('./routes/question');
var flow       = require('./routes/flow');
var functions  = require('./routes/functions');

QuestionProvider = function(host, port)
	{
       this.db= new Db('node-mongo-question', new Server(host, port, {safe: false}, {auto_reconnect: true}, {}));
       this.db.open(function(){});
     };

//Get Question Collection
QuestionProvider.prototype.getCollection= function(callback)
	{
       this.db.collection('questions', function(error, question_collection) {
                                                                              if( error ) callback(error);
                                                                              else callback(null, question_collection);
                                                                            });
     };

//Get flow collections
QuestionProvider.prototype.getCollection_flows= function(callback)
	{
       this.db.collection('flows', function(error, question_collection) {
                                                                              if( error ) callback(error);
                                                                              else callback(null, question_collection);
                                                                            });
     };




//find all questions
QuestionProvider.prototype.findAll = function(callback)
	       {
              this.getCollection(function(error, question_collection) {
                                                                         if( error ) callback(error)
                                                                         else {
                                                                                question_collection.find().sort({"_id":1}).toArray(function(error, results) {
                                                                                                                      if( error ) callback(error)
                                                                                                                      else callback(null, results)
                                                                                                                                            });
                                                                               }
                                                                       });
           };

//find all flows
QuestionProvider.prototype.findAll_flows = function(callback)
	       {
              this.getCollection_flows(function(error, flow_collection) {
                                                                         if( error ) callback(error)
                                                                         else {
                                                                                flow_collection.find().sort({"_id":1}).toArray(function(error, results) {
                                                                                                                      if( error ) callback(error)
                                                                                                                      else callback(null, results)
                                                                                                                                            });
                                                                               }
                                                                       });
           };


//save new question
QuestionProvider.prototype.save = function(questions, callback)
	{
      this.getCollection(function(error, question_collection) 
		                  {
                            if( error ) callback(error)
                            else
							 { 
                             //check for question with existing name....   
							question_collection.findOne({questionName: questions.questionName}, function(error, result)
											  { if( error ) callback(error)
											    else
												  {     // console.log(result);
														if(result==null)
															{ 
														          
																  if( typeof(questions.length)=="undefined")
                                                                       questions = [questions];
                                                                  for( var i =0;i< questions.length;i++ )
								                                   {
                                                                        question = questions[i];
                                                                        question.created_at = new Date();
                                                                   }
                                                                   question_collection.insert(questions, function() {callback(null, questions);});
														
														    }
															else
													         {														 
															  error = new Error();
															  error.msg='A Question with name \''+questions.questionName+'\' already exists.Please use a different Question Name.';															 
															  callback([error],null);
													         }
												  }
										      }); 
							    }
                          });
      };


//save new flow
QuestionProvider.prototype.save_flows = function(flows, callback)
	{
      this.getCollection_flows(function(error, flow_collection) 
		                  {
                            if( error ) callback(error)
                            else
							 { 
                             //check for flow with existing name....   
							flow_collection.findOne({flowName: flows.flowName}, function(error, result)
											  { if( error ) callback(error)
											    else
												  {     // console.log(result);
														if(result==null)
															{ 
														          
																  if( typeof(flows.length)=="undefined")
                                                                       flows = [flows];
                                                                  for( var i =0;i< flows.length;i++ )
								                                   {
                                                                        flow = flows[i];
                                                                        flow.created_at = new Date();
                                                                   }
                                                                   flow_collection.insert(flows, function() {callback(null, flows);});
														
														    }
															else
													         {														 
															  error = new Error();
															  error.msg='A Flow with name \''+flows.flowName+'\' already exists.Please use a different Flow Name.';															 
															  callback([error],null);
													         }
												  }
										      }); 
							    }
                          });
      };




//find an question by ID
QuestionProvider.prototype.findById = function(id, callback)
      {
	   this.getCollection(function(error, question_collection) 
		                           {
                                     if( error ) callback(error)
                                     else
									  {
                                         question_collection.findOne({_id: question_collection.db.bson_serializer.ObjectID.createFromHexString(id)}, function(error, result)
											  { if( error ) callback(error)
											    else callback(null, result)
										      });
									   }
									 });
	   };

//find an flow by ID
QuestionProvider.prototype.findById_flows = function(id, callback)
      {
	   this.getCollection_flows(function(error, flow_collection) 
		                           {
                                     if( error ) callback(error)
                                     else
									  {
                                         flow_collection.findOne({_id: flow_collection.db.bson_serializer.ObjectID.createFromHexString(id)}, function(error, result)
											  { if( error ) callback(error)
											    else callback(null, result)
										      });
									   }
									 });
	   };




// update an question
QuestionProvider.prototype.update = function(questionId, questions, callback) 
	   {
          this.getCollection(function(error, question_collection) 
			                     {
                                   if( error ) callback(error);
                                   else 
									{
                                       
									   //check for question with existing name....   
							           question_collection.findOne({questionName: questions.questionName}, function(error, result)
											  { if( error ) callback(error)
											    else
												  {      //console.log(result._id+':'+questionId);
														if(result==null || result._id==questionId)
															{ 
															question_collection.update({_id: question_collection.db.bson_serializer.ObjectID.createFromHexString(questionId)},
                                                                  questions,function(error, questions)
										                                      {
                                                                                 if(error) callback(error);
                                                                                 else callback(null, questions)       
                                                                               });
														    }
															else
													         {														 
															  error = new Error();
															  error.msg='A Question with name \''+questions.questionName+'\' already exists.Please use a different Question Name.';															 
															  callback([error],null);
													         }
												  }
										      }); 
									 }
                                   });
        };

// update a flow
QuestionProvider.prototype.update_flows = function(flowId, flows, callback) 
	   {
          this.getCollection_flows(function(error, flow_collection) 
			                     {
                                   if( error ) callback(error);
                                   else 
									{
                                       
									   //check for flow with existing name....   
							           flow_collection.findOne({flowName: flows.flowName}, function(error, result)
											  { if( error ) callback(error)
											    else
												  {      //console.log(result._id+':'+questionId);
														if(result==null || result._id==flowId)
															{ 
															flow_collection.update({_id: flow_collection.db.bson_serializer.ObjectID.createFromHexString(flowId)},
                                                                  flows,function(error, flows)
										                                      {
                                                                                 if(error) callback(error);
                                                                                 else callback(null, flows)       
                                                                               });
														    }
															else
													         {														 
															  error = new Error();
															  error.msg='A Flow with name \''+flows.flowName+'\' already exists.Please use a different Flow Name.';															 
															  callback([error],null);
													         }
												  }
										      }); 
									 }
                                   });
        };



//delete question
QuestionProvider.prototype.delete = function(questionId, callback)
	  {
       
		//console.log('In delete');
		this.getCollection(function(error, question_collection) 
			                 {
                                if(error) callback(error);
                                else 
								 {
                                  question_collection.remove({_id: question_collection.db.bson_serializer.ObjectID.createFromHexString(questionId)},
                                                             function(error, question){if(error) callback(error);else callback(null, question)});
                                 }
                             });
        };

		
//delete flow
QuestionProvider.prototype.delete_flows = function(flowId, callback)
	  {
       
		//console.log('In delete');
		this.getCollection_flows(function(error, flow_collection) 
			                 {
                                if(error) callback(error);
                                else 
								 {
                                  flow_collection.remove({_id: flow_collection.db.bson_serializer.ObjectID.createFromHexString(flowId)},
                                                             function(error, flow){if(error) callback(error);else callback(null, flow)});
                                 }
                             });
        };

//Delete Question by name.
QuestionProvider.prototype.deleteByName = function(name, callback)
	  {
       
		this.getCollection(function(error, question_collection) 
			                 {
                                if(error) callback(error);
                                else 
								 {
                                  
								  question_collection.remove({questionName: name},function(error, question){if(error) callback(error);else callback(question)});
                                 // console.log('Question Deleted :'+name);
                                 }
                             });
        };



//Export questions
QuestionProvider.prototype.export = function(callback)
           {
             
			      this.getCollection(function(error, question_collection)
					                   {
                                         if( error ) callback(error)
                                         else {
                                               question_collection.find().sort({"_id":1}).toArray(function(error, results) 
												   {
                                                     if( error ) callback(error)
                                                     else 
													   {
														 var stream = fileStream.createWriteStream(__dirname + '/Output/questiontest.js');
                                                         stream.once('open', function(fd) 
						                                 {
						                              		
															 stream.write('var utils = require(\'../lib/utils\');\n');
                                                             stream.write('var middleware = require(\'middleware\');\n');
                                                             stream.write('var BaseQuestion = middleware.models.BaseQuestion;\n\n');
                                                             stream.write('var questions = utils.map(BaseQuestion, [\n');															                      
															 var counter = 0;
                                                             results.forEach(function (question)
																              {																 
																                  if(counter == 0)
																				  stream.write('{header : {\n');
																				  else 
																				  stream.write(',\n{header : {\n');																				 
                                                                                  stream.write('            questionId:          '+counter                     +',\n');
                                                                                  stream.write('            questionName:       \''+question.questionName       +'\',\n');
                                                                                  stream.write('            questionType:       \''+question.questionType       +'\',\n');
                                                                                  stream.write('            questionDescription:\''+question.questionDescription+'\',\n');
                                                                                  stream.write('            questionCategory:   \''+question.questionCategory   +'\',\n');
                                                                                  stream.write('            questionText0:      \''+question.questionText0.replace(/\'/g, '\\\'')+'\',\n');
                                                                                  stream.write('            questionText1:      \''+question.questionText1.replace(/\'/g, '\\\'')+'\',\n');
                                                                                  stream.write('            questionText2:      \''+question.questionText2.replace(/\'/g, '\\\'')+'\',\n');                                                                                 
                                                                                  stream.write('            icon:               \''+question.icon               +'\',\n');
                                                                                  stream.write('            isQuestionEnabled:   '+question.isQuestionEnabled  +'\n');
                                                                                  stream.write('          },\n');																				  
																				  if (question.name0=='')
																				  stream.write(' data: [],\n');																		                                                                                        
																				  if (question.name0!='')
																				  {	
																				  stream.write(' data: [\n');
																				  stream.write('        {\n');
                                                                                  stream.write('            id:   \''+question.id0   +'\',\n');
                                                                                  stream.write('            name: \''+question.name0 +'\',\n');
                                                                                  stream.write('            value:\''+question.value0+'\',\n');
                                                                                  stream.write('            state:\''+question.state0+'\',\n');
                                                                                  stream.write('            type: \''+question.type0 +'\',\n');
                                                                                  stream.write('            width:\''+question.width0+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				  if (question.name1!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id1   +'\',\n');
                                                                                  stream.write('            name: \''+question.name1 +'\',\n');
                                                                                  stream.write('            value:\''+question.value1+'\',\n');
                                                                                  stream.write('            state:\''+question.state1+'\',\n');
                                                                                  stream.write('            type: \''+question.type1 +'\',\n');
                                                                                  stream.write('            width:\''+question.width1+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name2!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id2   +'\',\n');
                                                                                  stream.write('            name: \''+question.name2 +'\',\n');
                                                                                  stream.write('            value:\''+question.value2+'\',\n');
                                                                                  stream.write('            state:\''+question.state2+'\',\n');
                                                                                  stream.write('            type: \''+question.type2 +'\',\n');
                                                                                  stream.write('            width:\''+question.width2+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name3!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id3   +'\',\n');
                                                                                  stream.write('            name: \''+question.name3 +'\',\n');
                                                                                  stream.write('            value:\''+question.value3+'\',\n');
                                                                                  stream.write('            state:\''+question.state3+'\',\n');
                                                                                  stream.write('            type: \''+question.type3 +'\',\n');
                                                                                  stream.write('            width:\''+question.width3+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name4!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id4   +'\',\n');
                                                                                  stream.write('            name: \''+question.name4 +'\',\n');
                                                                                  stream.write('            value:\''+question.value4+'\',\n');
                                                                                  stream.write('            state:\''+question.state4+'\',\n');
                                                                                  stream.write('            type: \''+question.type4 +'\',\n');
                                                                                  stream.write('            width:\''+question.width4+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name5!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id5   +'\',\n');
                                                                                  stream.write('            name: \''+question.name5 +'\',\n');
                                                                                  stream.write('            value:\''+question.value5+'\',\n');
                                                                                  stream.write('            state:\''+question.state5+'\',\n');
                                                                                  stream.write('            type: \''+question.type5 +'\',\n');
                                                                                  stream.write('            width:\''+question.width5+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name6!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id6   +'\',\n');
                                                                                  stream.write('            name: \''+question.name6 +'\',\n');
                                                                                  stream.write('            value:\''+question.value6+'\',\n');
                                                                                  stream.write('            state:\''+question.state6+'\',\n');
                                                                                  stream.write('            type: \''+question.type6 +'\',\n');
                                                                                  stream.write('            width:\''+question.width6+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
                                                                                   if (question.name7!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id7   +'\',\n');
                                                                                  stream.write('            name: \''+question.name7 +'\',\n');
                                                                                  stream.write('            value:\''+question.value7+'\',\n');
                                                                                  stream.write('            state:\''+question.state7+'\',\n');
                                                                                  stream.write('            type: \''+question.type7 +'\',\n');
                                                                                  stream.write('            width:\''+question.width7+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name8!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id8   +'\',\n');
                                                                                  stream.write('            name: \''+question.name8 +'\',\n');
                                                                                  stream.write('            value:\''+question.value8+'\',\n');
                                                                                  stream.write('            state:\''+question.state8+'\',\n');
                                                                                  stream.write('            type: \''+question.type8 +'\',\n');
                                                                                  stream.write('            width:\''+question.width8+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				   if (question.name9!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id9   +'\',\n');
                                                                                  stream.write('            name: \''+question.name9 +'\',\n');
                                                                                  stream.write('            value:\''+question.value9+'\',\n');
                                                                                  stream.write('            state:\''+question.state9+'\',\n');
                                                                                  stream.write('            type: \''+question.type9 +'\',\n');
                                                                                  stream.write('            width:\''+question.width9+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				  	if (question.name10!='')
																				  {																				  
																				  stream.write('       ,{\n');
                                                                                  stream.write('            id:   \''+question.id10   +'\',\n');
                                                                                  stream.write('            name: \''+question.name10 +'\',\n');
                                                                                  stream.write('            value:\''+question.value10+'\',\n');
                                                                                  stream.write('            state:\''+question.state10+'\',\n');
                                                                                  stream.write('            type: \''+question.type10 +'\',\n');
                                                                                  stream.write('            width:\''+question.width10+'\'\n');                                                                                  
																				  stream.write('        }\n');
																				  }
																				  if (question.name0!='')
																				  stream.write('       ],\n');
																				 // stream.write(' questionSpecific: {'+question.questionSpecific+'}\n');
																				  stream.write(' questionSpecific: '+question.questionSpecific+'\n');
																				  stream.write('}\n');
																				  counter++;
																			  }); 
																	 stream.write(']);\n\nmodule.exports.questions = questions;');
                                                             stream.end();
                                                           });
														 callback(null, results)
													   }
                                                    });
                                               }
                                        })
			};



//Import questions
QuestionProvider.prototype.import = function(req,questionProvider,callback)
           {			
			fileStream.readFile(req.files.myfile.path,'utf8',function (err,data)				
			                              {
				                            var newPath = __dirname + "/Input/questions.js";
											var result = data.replace(/middleware.models.BaseQuestion/g, 'require(\'../models/basequestion\')');
											if(result.indexOf('questionName') > -1 && result.indexOf('isQuestionEnabled') > -1  && result.indexOf('questionSpecific') > -1) 
                                              {
                                                   fileStream.writeFile(newPath, result,'utf8', function (err)
												                   {
													                  if(err) callback(err);
													                  else callback();																		   
													                }); 
										      }
											 else callback(new Error());
										   });
										   
		   };



QuestionProvider.prototype.populateQuestions = function(questionProvider,callback)
{
     var _ = require('underscore');
	 //Utility for dynamic loading of modules
	 var hotload = require("hotload");
	 
	 //var contents = require(__dirname + '/Input/questions')
	 var contents = hotload("./Input/questions.js");
	 _.each(contents, function(output, name)
		 {
		   console.log('Inside first loop');
		  // console.log(JSON.stringify(output));
           _.each(output, function(model) 
			   {	  
			      var count = 0;
                  var id0='',name0='',value0='',state0='',type0='',width0='';var id1='',name1='',value1='',state1='',type1='',width1='';var id2='',name2='',value2='',state2='',type2='',width2='';
				  var id3='',name3='',value3='',state3='',type3='',width3='';var id4='',name4='',value4='',state4='',type4='',width4='';var id5='',name5='',value5='',state5='',type5='',width5='';
				  var id6='',name6='',value6='',state6='',type6='',width6='';var id7='',name7='',value7='',state7='',type7='',width7='';var id8='',name8='',value8='',state8='',type8='',width8='';
				  var id9='',name9='',value9='',state9='',type9='',width9='';var id10='',name10='',value10='',state10='',type10='',width10='';
				  _.each(model.data,function(mydata)
					{																										
					  if (count == 0)
					    {id0    = mydata.id;name0  = mydata.name;value0 = mydata.value;state0 = mydata.state;type0  = mydata.type;width0 = mydata.width;}
					  if (count == 1)
						{id1    = mydata.id;name1  = mydata.name;value1 = mydata.value;state1 = mydata.state;type1  = mydata.type;width1 = mydata.width;}
					  if (count == 2)
						{id2    = mydata.id;name2  = mydata.name;value2 = mydata.value;state2 = mydata.state;type2  = mydata.type;width2 = mydata.width;}
					  if (count == 3)
						{id3    = mydata.id;name3  = mydata.name;value3 = mydata.value;state3 = mydata.state;type3  = mydata.type;width3 = mydata.width;}
					  if (count == 4)
						{id4    = mydata.id;name4  = mydata.name;value4 = mydata.value;state4 = mydata.state;type4  = mydata.type;width4 = mydata.width;}
					  if (count == 5)
						{id5    = mydata.id;name5  = mydata.name;value5 = mydata.value;state5 = mydata.state;type5  = mydata.type;width5 = mydata.width;}
					  if (count == 6)
						{id6    = mydata.id;name6  = mydata.name;value6 = mydata.value;state6 = mydata.state;type6  = mydata.type;width6 = mydata.width;}
					  if (count == 7)
						{id7    = mydata.id;name7  = mydata.name;value7 = mydata.value;state7 = mydata.state;type7  = mydata.type;width7 = mydata.width;}
					  if (count == 8)
						{id8    = mydata.id;name8  = mydata.name;value8 = mydata.value;state8 = mydata.state;type8  = mydata.type;width8 = mydata.width;}
					  if (count == 9)
						{id9    = mydata.id;name9  = mydata.name;value9 = mydata.value;state9 = mydata.state;type9  = mydata.type;width9 = mydata.width;}
					  if (count == 10)
						{id10    = mydata.id;name10  = mydata.name;value10 = mydata.value;state10 = mydata.state;type10  = mydata.type;width10 = mydata.width;}
 					  count=count+1;				 
					 });
					questionProvider.deleteByName(model.header.questionName,function (result){console.log(result)});		   
		            questionProvider.save({questionName:  model.header.questionName,questionType:  model.header.questionType,questionDescription:  model.header.questionDescription,
								           questionCategory:  model.header.questionCategory,questionText0:  model.header.questionText0,questionText1:  model.header.questionText1,
								           questionText2:  model.header.questionText2,icon:  model.header.icon,isQuestionEnabled:  model.header.isQuestionEnabled,
						                   id0:id0,name0:name0,value0:value0,state0:state0,type0:type0,width0:width0,
						                   id1:id1,name1:name1,value1:value1,state1:state1,type1:type1,width1:width1,
								           id2:id2,name2:name2,value2:value2,state2:state2,type2:type2,width2:width2,
						                   id3:id3,name3:name3,value3:value3,state3:state3,type3:type3,width3:width3,
								           id4:id4,name4:name4,value4:value4,state4:state4,type4:type4,width4:width4,
						                   id5:id5,name5:name5,value5:value5,state5:state5,type5:type5,width5:width5,
								           id6:id6,name6:name6,value6:value6,state6:state6,type6:type6,width6:width6,
								           id7:id7,name7:name7,value7:value7,state7:state7,type7:type7,width7:width7,
								           id8:id8,name8:name8,value8:value8,state8:state8,type8:type8,width8:width8,
								           id9:id9,name9:name9,value9:value9,state9:state9,type9:type9,width9:width9,
								           id10:id10,name10:name10,value10:value10,state10:state10,type10:type10,width10:width10,
								           questionSpecific:require('util').inspect(model.questionSpecific, {depth:null})
						                   }, function( error,docs){}
					                     );
			 }); 
		  });
	// delete require.cache[contents];	
     callback();
	 
};



//Export questions
QuestionProvider.prototype.export_flow = function(id,callback)
           {
             
			      this.getCollection_flows(function(error, flow_collection)
					                   {
                                         if( error ) callback(error)
                                         else {
                                               flow_collection. findOne({_id: flow_collection.db.bson_serializer.ObjectID.createFromHexString(id)},function(error, results)
												  
												   {
                                                     if( error ) callback(error)
                                                     else 
													   {
														 /*
														 console.log(results);
														 if(results.todaysData_Enabled==true)
															 {  console.log('reads boolean');}
														  if(results.todaysData_Enabled=='true')
															 {  console.log('reads value');}
														 */
														 
														 var stream = fileStream.createWriteStream(__dirname + '/Output/Flows/'+results.flowName+'.nools');
                                                         stream.once('open', function(fd) 
						                                   {
						                              		 stream.write('define RuleInput     {                          \n');
															 stream.write('                       todaysData: [],          \n');
															 stream.write('                       weeksData: [],           \n');
															 stream.write('                       usersCompetitionData: [],\n');
															 stream.write('                       dayInWeek: 0,            \n');
															 stream.write('                       challengeDefinitionId: 0,\n');
															 stream.write('                       userId: 0,               \n');
															 stream.write('                       teamId: null,            \n');
															 stream.write('                       competitionId: 0,        \n');
															 stream.write('                       rewards: 0               \n');
															 stream.write('                     }                         \n');

                                                             stream.write('define EvalResults   {                                \n');
															 stream.write('                       rewards: [],                    \n');
															 stream.write('                       addReward: function(reward)  {  \n');
															 stream.write('                                                       this.reward.push(reward); \n');
															 stream.write('                                                    }, \n');
															 stream.write('                       constructor: function(reward){\n');
															 stream.write('                                                       this.reward.push(reward);\n');
															 stream.write('                                                    }\n');
															 stream.write('                      }                             \n');

															 stream.write('define SuccessCounter {                               \n');
															 stream.write('                       count: 0,                     \n');
															 stream.write('                       constructor: function(count){ \n');
															 stream.write('                                                     this.count = count; \n');
															 stream.write('                                                    } \n');
															 stream.write('                      }                               \n');
															 stream.write('\n');
                                                             if(results.todaysData_Enabled=='true')
															 {
                                                              stream.write('rule "'+results.ruleName_todaysData+'" \n');
															  stream.write('{\n');
															  stream.write('  when {             \n');
															  stream.write('         m: RuleInput  ');
															  if (results.todaysData!='')
																 {
																  stream.write('m.todaysData '+results.todaysData_Condition+' '+results.todaysData);
																  if (results.todaysDataLength!='')stream.write(' && ');
																 }
                                                              if (results.todaysDataLength!='')
															  stream.write('m.todaysData.length '+results.todaysDataLength_Condition+' '+results.todaysDataLength);
															  stream.write(';\n'); 
															  stream.write('       }\n');
															  stream.write('  then {\n'); 
															  stream.write('         console.log(\' Today Data Rule\');\n');
															  stream.write('         m.reward = '+results.reward_todaysData+';\n');
															  stream.write('         emit("'+results.emitEvent_todaysData+'",m);\n');
                                                              stream.write('       }\n');
															  stream.write('}\n');
															 }

                                                              stream.write('\n');
															      if(results.weeksData_Enabled=='true')
															 {
                                                              stream.write('rule "'+results.ruleName_weeksData+'" \n');
															  stream.write('{\n');
															  stream.write('  when {             \n');
															  stream.write('         m: RuleInput  ');
															  if (results.dayInWeek!='')
																 {
																  stream.write('m.dayInWeek '+results.dayInWeek_Condition+' '+results.dayInWeek);
																  if (results.weekLength!='')stream.write(' && ');
																 }
                                                              if (results.weekLength!='')
															  stream.write('m.weeksData.length '+results.lenght_Condition+' '+results.weekLength);
															  stream.write(';\n'); 
															  stream.write('       }\n');
															  stream.write('  then {\n');
															  stream.write('         console.log(\' Week Data Rule\');\n');
															  if(results.dataResult!='')
															 {stream.write('         loop(m.weeksData, function(data){\n');
															  stream.write('             if(data.result '+results.dataResult_Condition+' '+results.dataResult+'){\n');
															  stream.write('                 counter.tick();\n');
															  stream.write('                }\n');
															  stream.write('         });\n');
															  stream.write('         if(counter.count >= 6){\n');
															  stream.write('              m.reward = '+results.reward_weeksData+';\n');
															  stream.write('              emit("'+results.emitEvent_weeksData+'",m);\n');
															  stream.write('         }\n');
															 }
															 else 
															 {stream.write('         m.reward = '+results.reward_weeksData+';\n');
															  stream.write('         emit("'+results.emitEvent_weeksData+'",m);\n');
															 }
                                                              stream.write('       }\n');
                                                             }
                                                              stream.write('\n');
                                                              if(results.competitionData_Enabled=='true')
															 {
                                                              stream.write('rule "'+results.ruleName_competitionData+'" \n');
															  stream.write('{\n');
															  stream.write('  when {             \n');
															  stream.write('         m: RuleInput  m.usersCompetitionData '+results.usersCompetitionData_Condition+' '+results.usersCompetitionData+';\n');													  
															  stream.write('       }\n');
															  stream.write('  then {\n'); 
															  stream.write('         console.log(\' Competition Data Rule\');\n');
															  stream.write('         m.reward = '+results.reward_competitionData+';\n');
															  stream.write('         emit("'+results.emitEvent_competitionData+'",m);\n');
                                                              stream.write('       }\n');
															  stream.write('}\n');
															 }                                             
                                                            stream.end();   
                                                            });
                                                          
														  callback(null, results)
                                                       }														 
												   });
                                              }
                                       });                                      
			};
			
//Fetch available libraries
QuestionProvider.prototype.getLibraries = function(callback)
{
     var _ = require('underscore'); 
	 var hotload = require("hotload");	 
	 var contents = hotload("./FlowInput/Example.js");
     console.log('loaded the config file....');	
     var libraries =[];
	 _.each(contents, function(output, name)
		 {
		   console.log('Inside first loop');		 		   
		   var count = 0;
           _.each(output, function(model) 
			   {
			     if(model.libraryName!=null)
				  {
	                libraries[count] = model.libraryName
					count++;
				  }								
			 });			 
		  });
	 console.log(libraries);
     callback(null,libraries);	 
};

//Fetch all the categories for a library
QuestionProvider.prototype.getCategoriesFromLibrary = function(lbrName,callback)
{
     var _ = require('underscore');	 
	 var hotload = require("hotload"); 
	 var contents = hotload("./FlowInput/Example.js");
     console.log('loading categories....');	
     var categories =[];
	 _.each(contents, function(alllibraries, name)
		 {
		   console.log('Inside first loop');		   
           _.each(alllibraries, function(library) 
			   {	  
			     if (library.libraryName==lbrName)
					{						 
						 _.each(library,function(allCategories)
					     {																										
					          var count = 0;
							  _.each(allCategories,function(category)
					         {	
								if(category.categoryName!=null)
								 {
								   categories[count] = category.categoryName;
								   count++;
								 }
						     }); 
							 					  				 
					      });
					}			
			 });			 
		  });
	 //console.log(categories);
     callback(null,categories);
	 
};

//Fetch all the functions for a category in a library
QuestionProvider.prototype.getFunctionsFromCategory = function(lbrName,ctgyName,callback)
{
     var _ = require('underscore');	 
	 var hotload = require("hotload"); 
	 var contents = hotload("./FlowInput/Example.js");
     console.log('loading categories....');	
     var functions =[];
	 _.each(contents, function(alllibraries, name)
		 {
		   console.log('Inside first loop');		   
           _.each(alllibraries, function(library) 
			   {	  
			     if (library.libraryName==lbrName)
					{						 
						 _.each(library,function(allCategories)
					     {										
					         
							  _.each(allCategories,function(category)
					         {	
								if(category.categoryName==ctgyName)
								 {
								   _.each(category,function(allfunctions)
					                {
                                       var count = 0;
									   _.each(allfunctions,function(myfunction)
					                        {
                                                if(myfunction.functionName!=null) 
												{
												  functions[count] = myfunction.functionName;
								                  count++;
												}


											});

									});
								 }
						     }); 
							 					  				 
					      });
					}			
			 });			 
		  });
	 console.log(functions);
     callback(null,functions);
	 
};


//Fetch paramName of a functions for a category in a library
QuestionProvider.prototype.getFunctionsParams = function(lbrName,ctgyName,fnName,callback)
{
     var _ = require('underscore');	 
	 var hotload = require("hotload"); 
	 var contents = hotload("./FlowInput/Example.js");
     console.log('loading categories....');	
     var paramName ='';
	 var paramType = '';  //integer, string, float, date, etc
     var defaultValue = '';
	 _.each(contents, function(alllibraries, name)
		 {
		   console.log('Inside first loop');		   
           _.each(alllibraries, function(library) 
			   {	  
			     if (library.libraryName==lbrName)
					{						 
						 _.each(library,function(allCategories)
					     {										
					         
							  _.each(allCategories,function(category)
					         {	
								if(category.categoryName==ctgyName)
								 {
								   _.each(category,function(allfunctions)
					                {
                                       
									   _.each(allfunctions,function(myfunction)
					                        {
                                                if(myfunction.functionName==fnName) 
												{
												  paramName    = myfunction.params[0].paramName;
												  paramType    = myfunction.params[0].paramType;
												  defaultValue = myfunction.params[0].defaultValue;
								                  
												}

											});

									});
								 }
						     }); 
							 					  				 
					      });
					}			
			 });			 
		  });
	 console.log(paramName);
     callback(null,paramName,paramType,defaultValue);
	 
};





exports.QuestionProvider = QuestionProvider;