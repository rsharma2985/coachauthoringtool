//example

module.exports.libraries=  
 [
    
	   {libraryName: 'user',
        categories: 
		   [
			   {categoryName: 'User Functions',
                functions: 
				  [
                    
						{  functionName: 'adduser',
                           params: [ 
							         {
                                        paramName: 'userId',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: '123'

                                     }
                                   ]
						},
					    {  functionName: 'deleteuser',
                           params: [
                                     {
                                        paramName: 'userName',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: 'Rahul'
                                     }
                                   ]
						}				
					
                  ]
			   },

               {categoryName: 'User Views',
                functions: 
				  [
                    
						{  functionName: 'fetchSingleUser',
                           params: [ 
							         {
                                        paramName: 'userId',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: '456'
                                     }
                                   ]
						},
					    {  functionName: 'fetchAllUsers',
                           params: [
                                     {
                                        paramName: 'count',
                                        paramType: 'integer',  //integer, string, float, date, etc
                                        defaultValue: 'Amey'
                                     }
                                   ]
						}				
					
                  ]
			   }

            ]
       },


       {libraryName: 'Questions',
        categories: 
		   [
			   {categoryName: 'Question Functions',
                functions: 
				  [
                    
						{  functionName: 'addquestion',
                           params: [ 
							         {
                                        paramName: 'questionId',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: '100'
                                     }
                                   ]
						},
					    {  functionName: 'deletequestion',
                           params: [
                                     {
                                        paramName: 'questionName',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: 'Greetings'
                                     }
                                   ]
						}				
					
                  ]
			   },

               {categoryName: 'Question Views',
                functions: 
				  [
                    
						{  functionName: 'fetchSingleQuestion',
                           params: [ 
							         {
                                        paramName: 'questionId',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: '200'
                                     }
                                   ]
						},
					    {  functionName: 'fetchAllQuestions',
                           params: [
                                     {
                                        paramName: 'count',
                                        paramType: 'integer',  //integer, string, float, date, etc
                                        defaultValue: '10'
                                     }
                                   ]
						}				
					
                  ]
			   }

            ]
       },

       {libraryName: 'Rule',
        categories: 
		   [
			   {categoryName: 'Rule Functions',
                functions: 
				  [
                    
						{  functionName: 'addrule',
                           params: [ 
							         {
                                        paramName: 'ruleId',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: '45'
                                     }
                                   ]
						},
					    {  functionName: 'deleterule',
                           params: [
                                     {
                                        paramName: 'ruleName',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: 'ChronicIllness'
                                     }
                                   ]
						}				
					
                  ]
			   },

               {categoryName: 'Rule Views',
                functions: 
				  [
                    
						{  functionName: 'fetchSingleRule',
                           params: [ 
							         {
                                        paramName: 'ruleId',
                                        paramType: 'string',  //integer, string, float, date, etc
                                        defaultValue: '50'
                                     }
                                   ]
						},
					    {  functionName: 'fetchAllRules',
                           params: [
                                     {
                                        paramName: 'count',
                                        paramType: 'integer',  //integer, string, float, date, etc
                                        defaultValue: '10'
                                     }
                                   ]
						}				
					
                  ]
			   }

            ]
       }

       

   
]